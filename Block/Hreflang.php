<?php


namespace M21\Hreflang\Block;

use Magento\Framework\View\Element\Template;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\Group;
use Magento\Store\Model\Store;
use Magento\Store\Model\Website;

class Hreflang extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \M21\Hreflang\Service\Hreflang\AlternativeUrlService
     */
    protected $alternativeUrlService;
    /**
     * @var Magento\Framework\Locale\Resolver
     */
    protected $store;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \M21\Hreflang\Service\Hreflang\AlternativeUrlService $alternativeUrlService,
        \Magento\Framework\Locale\Resolver $store,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->alternativeUrlService = $alternativeUrlService;
        $this->store = $store;
    }

    /**
     * @return string
     */
    public function getHreflang()
    {
        $data = [];
        $currentStore = substr($this->getCurrentLanguageCode(), 0, 2);
        foreach ($this->getStores() as $store) {
            $url = $this->getStoreUrl($store);
            if ($url) {
                $code = $this->getLocaleCode($store);
                if ($currentStore != $code) {
                    $data[$code] = $url;
                }
            }
        }
        return $data;
    }

    public function getCurrentLanguageCode()
    {
        return $this->store->getLocale();
    }


    /**
     * @param Store $store
     * @return string
     */
    private function getStoreUrl($store)
    {
        return $this->alternativeUrlService->getAlternativeUrl($store);
    }

    /**
     * @param StoreInterface $store
     * @return bool
     */
    private function isCurrentStore($store)
    {
        return $store->getId() == $this->_storeManager->getStore()->getId();
    }

    /**
     * @param StoreInterface $store
     * @return string
     */
    private function getLocaleCode($store)
    {
        if ($this->_scopeConfig->getValue('general/locale/set_locale_with_region', 'stores', $store->getId())) {
            return str_replace('_', '-', strtolower($this->_scopeConfig->getValue('general/locale/code', 'stores', $store->getId())));
        } else {
            return strtolower(substr($this->_scopeConfig->getValue('general/locale/code', 'stores', $store->getId()), 0, 2));
        }
    }

    /**
     * @return Store[]
     */
    private function getStores()
    {
        return $this->_storeManager->getStores();
    }


}
